<?php
declare (strict_types=1);

namespace mark\notice;

final class Message {
    private $message = array();

    public static $type_text = 'text';
    public static $type_image = 'image';
    public static $type_voice = 'voice';
    public static $type_video = 'video';
    public static $type_music = 'music';
    public static $type_link = 'link';
    public static $type_notice = 'notify';

    public function __construct() {

    }

    /**
     * 发送文本消息
     *
     * @param string $text
     *
     * @return $this
     */
    public function setTextMsg(string $text): self {
        $this->message['type'] = self::$type_text;
        $this->message['text']['content'] = $text;
        return $this;
    }

    /**
     * 发送图片消息
     *
     * @param string $media_id
     *
     * @return $this
     */
    public function setImageMsg(string $media_id): self {
        $this->message['type'] = self::$type_image;
        $this->message['image']['media_id'] = $media_id;
        return $this;
    }

    /**
     * 发送语音消息
     *
     * @param string $media_id
     *
     * @return $this
     */
    public function setVoiceMsg(string $media_id): self {
        $this->message['type'] = self::$type_voice;
        $this->message['voice']['media_id'] = $media_id;
        return $this;
    }

    /**
     * 发送视频消息
     *
     * @param string $media_id
     * @param string $title
     * @param string $description
     *
     * @return $this
     */
    public function setVideoMsg(string $media_id, string $title, string $description): self {
        $this->message['type'] = self::$type_video;
        $this->message['video']['media_id'] = $media_id;
        $this->message['video']["thumb_media_id"] = $media_id;
        $this->message['video']["title"] = $title;
        $this->message['video']["description"] = $description;
        return $this;
    }

    /**
     * 发送音乐消息
     *
     * @param string $title
     * @param string $description
     * @param        $media_id
     * @param string $url
     * @param string $hq_url
     * @param        $thumb_media_id
     *
     * @return $this
     */
    public function setMusicMsg(string $title, string $description, $media_id, string $url, string $hq_url, $thumb_media_id): self {
        $this->message['type'] = self::$type_music;
        $this->message['music']["title"] = $title;
        $this->message['music']["description"] = $description;
        $this->message['music']['media_id'] = $media_id;
        $this->message['music']["url"] = $url;
        $this->message['music']["hqurl"] = $hq_url;
        $this->message['music']["thumb_media_id"] = $thumb_media_id;
        return $this;
    }

    /**
     * 发送通知消息
     *
     * @param string $title
     * @param string $first
     * @param string $message
     * @param string $url
     * @param string $remark
     * @param int    $time
     *
     * @return $this
     */
    public function setNoticeMsg(string $title, string $first, string $message, string $url = '', string $remark = '', int $time = 0): self {
        $this->message['type'] = self::$type_notice;
        $this->message['notify']["title"] = $title;
        $this->message['notify']["first"] = $first;
        $this->message['notify']["message"] = $message;
        $this->message['notify']["url"] = $url;
        $this->message['notify']["time"] = date('Y-m-d H:i:s', $time === 0 ?: time());
        $this->message['notify']["remark"] = $remark;

        return $this;
    }

    /**
     * 发送连接消息
     *
     * @param string $title       消息标题
     * @param string $description 链接描述
     * @param string $url         链接消息被点击后跳转的链接
     * @param string $thumb_url   链接消息的图片链接，支持 JPG、PNG 格式，较好的效果为大图 640 X 320，小图 80 X 80
     *
     * @return $this
     */
    public function setLinkMsg(string $title, string $description, string $url, string $thumb_url): self {
        $this->message['type'] = self::$type_link;
        $this->message['link']["title"] = $title;
        $this->message['link']["description"] = $description;
        // $this->message['music']['media_id'] = $media_id;
        $this->message['link']["url"] = $url;
        $this->message['link']["thumb_url"] = $thumb_url;
        // $this->message['music']["thumb_media_id"] = $thumb_media_id;
        return $this;
    }

    public function getMessage(): array {
        return $this->message;
    }

}