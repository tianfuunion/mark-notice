<?php
declare (strict_types=1);

namespace mark\notice;

use mark\auth\Authorize;
use mark\http\Curl;
use mark\system\Os;

/**
 * Class Notify
 *
 * @package mark\notice
 */
class Notify {
    private $appid;
    private $secret;
    public static $host = 'https://notify.tianfu.ink';

    public function __construct($appid, $secret) {
        if (!empty($appid) && !empty($secret)) {
            $this->appid = $appid;
            $this->secret = $secret;
        }
    }

    /**
     * 通知发送者
     *
     * @param string               $access_token
     * @param string               $receiver
     * @param string               $sender
     * @param \mark\notice\Message $message
     *
     * @return array
     */
    public function sender(string $access_token, string $receiver, string $sender, Message $message): array {
        $result = Curl::getInstance()
            ->post(self::$host . '/api.php/message/receiver?access_token=' . $access_token, 'json')
            ->appendData('appid', $this->appid)
            ->appendData('secret', $this->secret)
            ->appendData('receiver', $receiver)
            ->appendData('sender', $sender)
            ->appendData('createtime', time())
            ->appendData('createip', Os::getIpvs())
            ->appendData('message', $message->getMessage())
            ->addHeader('auth-type', Authorize::$_type)
            ->addHeader('auth-version', Authorize::$_version)
            ->toArray();

        if (!empty($result) && isset($result['code']) && $result['code'] == 200 && isset($result['data']) && !empty($result['data'])) {
            return $result['data'] ?: array();
        }

        return $result ?: array();
    }

    /**
     * 通知接收者
     *
     */
    public function receiver() {

    }

    public function handler() {

    }

    /**
     * 运行事件预警
     *
     * @param string $token
     * @param        $appid
     * @param string $secret
     * @param string $receiver
     * @param string $sender
     * @param string $title
     * @param string $first
     * @param string $msg
     * @param string $url
     * @param string $remark
     * @param int    $time
     *
     * @return array
     */
    public static function runevent(string $token, $appid, string $secret, string $receiver, string $sender, string $title, string $first, string $msg, string $url = '',
                                    string $remark = '', int $time = 0) {
        if ($time == 0) {
            $time = time();
        }
        $message = new Message();
        $message->setNoticeMsg($title, $first, $msg, $url, $remark, $time);
        $notify = new self($appid, $secret);
        return $notify->sender($token, $receiver, $sender, $message);
    }

}