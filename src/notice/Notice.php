<?php
declare (strict_types=1);

namespace mark\notice;

use Exception;
use mark\http\Curl;
use mark\system\Os;

final class Notice {
    protected $appid;
    protected $secret;

    const TEXT = 'text';
    const VIDEO = 'video';
    const AUDIO = 'audio';
    const IMAGE = 'image';
    const APP = 'application';
    const LINK = 'link';
    const LOCATION = 'location';

    private $msgType = self::TEXT;

    const CHARSET_UTF8 = 'utf-8';
    const ENCODING_BASE64 = 'base64';

    /**
     * SMTP hosts.
     * Either a single hostname or multiple semicolon-delimited hostnames.
     * You can also specify a different port
     * for each host by using this format: [hostname:port]
     * (e.g. "smtp1.example.com:25;smtp2.example.com").
     * You can also specify encryption type, for example:
     * (e.g. "tls://smtp1.example.com:587;ssl://smtp2.example.com:465").
     * Hosts will be tried in order.
     *
     * @var string
     */
    private $host = 'https://notify.tianfu.ink';

    /**
     * The SMTP server timeout in seconds.
     * Default of 5 minutes (300sec) is from RFC2821 section 4.5.3.2.
     *
     * @var int
     */
    private $timeout = 300;

    private $receiver;
    private $sender;

    private $expiretime = 0;
    private $subject = '';

    private $header = array();
    private $content = '';

    public function __construct($appid, $secret) {
        if (!empty($appid) && !empty($secret)) {
            $this->appid = $appid;
            $this->secret = $secret;
        }
    }

    /**
     * 构建消息内容
     *
     * @return array
     */
    private function buildContent() {
        $this->content = array(
            'noticeId' => 202008121720, // 通知ID

            'sender' => $this->sender, // 发送者ID

            'msgtype' => $this->msgType, // 消息类型
            'receiver' => $this->receiver, // 接收者ID
            'expiretime' => $this->expiretime, // 过期日间

            'header' => array(),

            'body' => '需要发送的数据',

            'token' => '', // 令牌
            'code' => '', // 校验码

            'server' => $_SERVER, // 系统环境
            'subid' => $_SESSION['uid'], // 提交者ID
            'subip' => Os::getIpvs(), // 提交IP
            'subtime' => time(), // 提交时间

            // 定位数据
            'location' => array('latitude' => '纬度', 'longitude' => '经度', 'speed' => '速度', 'accuracy' => '位置精度'),

            'size' => 0,// 内容大小
            'status' => 1,  // 消息状态：0、无效，1、未读，2、已读，3、未定义，4、已删除
        );

        return $this->content;
    }

    /**
     * 格式化数据内容
     *
     * @return string
     */
    private function getContent() {
        $json = json_encode($this->content, JSON_UNESCAPED_UNICODE);
        if (!is_bool($json) && $json !== false) {
            return (string)$json;
        }
        return '';
    }

    /**
     * Add an attachment from a path on the filesystem.
     * Never use a user-supplied path to a file!
     * Returns false if the file could not be found or read.
     * Explicitly *does not* support passing URLs; PHPMailer is not an HTTP client.
     * If you need to do that, fetch the resource yourself and pass it in via a local file or string.
     *
     * @param string $path        Path to the attachment
     * @param string $name        Overrides the attachment name
     * @param string $encoding    File encoding (see $Encoding)
     * @param string $type        File extension (MIME) type
     * @param string $disposition Disposition to use
     *
     * @return bool
     * @throws Exception
     */
    public function addAttachment($path, $name = '', $encoding = self::ENCODING_BASE64, $type = '', $disposition = 'attachment') {

        return false;
    }

    /**
     * @return array|bool|false|string
     */
    private function preSend() {
        $curl = Curl::getInstance()
            ->post($this->host)
            ->append($this->getContent());

        $result = $curl->execute();
        $code = $curl->getResponseCode();
        if ($code === 200) {
            return $curl->toArray();
        }
        $this->setError($result);

        return $result;
    }

    private function postSend() {
        try {
            // Choose the mailer and send through it
            switch ($this->msgType) {
                case self::TEXT:
                    return $this->sendText($this->MIMEHeader, $this->MIMEBody);
                    break;
                case self::IMAGE:
                    return $this->sendImage($this->MIMEHeader, $this->MIMEBody);
                    break;
                case self::AUDIO:
                    return $this->sendAudio($this->MIMEHeader, $this->MIMEBody);
                    break;
                case self::VIDEO:
                    return $this->sendVideo($this->MIMEHeader, $this->MIMEBody);
                    break;
                case self::APP:
                    return $this->sendApp($this->MIMEHeader, $this->MIMEBody);
                    break;
                default:
                    break;
            }
        } catch (Exception $e) {
            $this->setError($e->getMessage());
        }

        return false;
    }

    /**
     * 发送通知消息
     * Create a message and send it.
     * Uses the sending method specified by $Mailer.
     *
     * @return bool false on error - See the ErrorInfo property for details of the error
     * @throws Exception
     */
    public function send() {
        try {
            if (!$this->preSend()) {
                return false;
            }

            return $this->postSend();
        } catch (Exception $exc) {
            $this->mailHeader = '';
            $this->setError($exc->getMessage());
            return false;
        }
    }

    /**
     * Holds the most recent mailer error message.
     *
     * @var string
     */
    public $ErrorInfo = '';

    /**
     * Add an error message to the error container.
     *
     * @param string $msg
     */
    protected function setError($msg) {
        $this->ErrorInfo = $msg;
    }

    /**
     * 设置发送者
     *
     * @param string $string
     *
     * @return $this
     */
    public function setReceiver(string $receiver) {
        $this->receiver = $receiver;
        return $this;
    }

    /**
     * @param string $sender
     *
     * @return $this
     */
    public function setSender(string $sender) {
        $this->sender = $sender;
        return $this;
    }

    /**
     * @param string $subject
     *
     * @return $this
     */
    public function setSubject(string $subject) {
        $this->subject = $subject;
        return $this;
    }

    /**
     * @param string $type
     *
     * @return $this
     */
    public function setMsgType(string $type) {
        $this->msgType = $type;
        return $this;
    }

    /**
     * @param int $expiretime
     *
     * @return $this
     */
    public function setExpiretime(int $expiretime) {
        $this->expiretime = $expiretime;
        return $this;
    }

    /**
     * @param string $host
     *
     * @return $this
     */
    public function setHost(string $host) {
        $this->host = $host;
        return $this;
    }

    /**
     * 设置超时时间
     *
     * @param int $time
     *
     * @return $this
     */
    public function setTimeout(int $time = 300) {
        $this->timeout = $time;
        return $this;
    }
}